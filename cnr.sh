CLASSPATH=./lib/json.jar:./bin

javac -d "./bin" -cp $CLASSPATH ./src/kiik3D/types/*.java ./src/kiik3D/components/*.java ./src/kiik3D/*.java ./src/kiik3D/objects/*.java ./src/kiik3D/input/*.java
java -cp $CLASSPATH kiik3D.Main
