package kiik3D.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.io.UnsupportedEncodingException;

import kiik3D.objects.Entity;
import kiik3D.components.WASDMovement;
import kiik3D.types.Face;
import kiik3D.types.Material;
import kiik3D.types.Mesh;
import kiik3D.types.Vertex;
import kiik3D.util.TextureReader;







import com.jogamp.opengl.util.texture.Texture;

import kiik3D.util.ResourceList;

import java.util.regex.Pattern;
import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Babylonloader {


	public static Mesh loadModel(String fn) throws JSONException {
        File file = new File(fn);
        FileInputStream fis;
        byte[] data;
        Mesh mesh = new Mesh();

        try {
            fis = new FileInputStream(file);
            data = new byte[(int)file.length()];
            try {
                fis.read(data);
            } catch (IOException exc) {
                System.out.println(exc);
                return null;
            }

            try {
                String s = new String(data, "UTF-8");
                //System.out.println(s+" TESTEST");
                mesh = parseBabylonModelData(s);
                try {
                    fis.close();
                } catch (IOException exc) {
                    System.out.println(exc);
                }
            } catch (UnsupportedEncodingException exc) {
                System.out.println(exc);
            }
        } catch (FileNotFoundException exc) {
            System.out.println(exc);
        }
        return mesh;
    }
	
	private static Mesh parseBabylonModelData(String data) throws JSONException {
        JSONObject o = new JSONObject(data);

        ArrayList<Mesh> meshes = new ArrayList<Mesh>();
        ArrayList<Material> materials = new ArrayList<Material>();

        JSONArray jsonMaterials = o.getJSONArray("materials");
        for(int i=0;i<jsonMaterials.length();i++) {
            JSONObject mat = jsonMaterials.getJSONObject(i);
            String name = mat.getString("name");
            String ID = mat.getString("id");

            //TODO: check if exists
            JSONObject diffuseTexture = mat.getJSONObject("diffuseTexture");
            String diffuseTextureName = diffuseTexture.getString("name");
            System.out.println(name+ID+diffuseTexture);
            materials.add(new Material(ID, name, diffuseTextureName));
        }

        JSONArray  jsonMeshes = o.getJSONArray("meshes");
        for(int i=0;i<jsonMeshes.length();i++) {
            if(i==0) {
                JSONObject jMesh = jsonMeshes.getJSONObject(i);

                JSONArray vertices = (JSONArray)jMesh.get("vertices");
                JSONArray indices = (JSONArray)jMesh.get("indices");

                int uvCount = (int)jMesh.getInt("uvCount");
                int verticesStep = 1;

                switch(uvCount) {
                    case 0:
                        verticesStep = 6;
                        break;
                    case 1:
                        verticesStep = 8;
                        break;
                    case 2:
                        verticesStep = 10;
                        break;
                }

                int verticesCount = vertices.length() / verticesStep;
                int faceCount = indices.length() / 3;

                Mesh m = new Mesh(verticesCount, faceCount);

                for(int b=0;b<verticesCount;b++) {
                    int _b = b * verticesStep;
                    double x = (double)vertices.getDouble(_b++);
                    double y = (double)vertices.getDouble(_b++);
                    double z = (double)vertices.getDouble(_b++);
                    double nx = (double)vertices.getDouble(_b++);
                    double ny = (double)vertices.getDouble(_b++);
                    double nz = (double)vertices.getDouble(_b++);

                    Vertex vert = new Vertex(x, y, z);
                    vert.setNormal(nx, ny, nz);

                    if (uvCount > 0) {
                        double u = (double)vertices.getDouble(_b++);
                        double v = (double)vertices.getDouble(_b++);
                        vert.setUV(u, v);
                    }

                    m.vertices[b] = vert;
                }



                for(int x=0;x<faceCount;x++) {
                    int _x = x * 3;
                    int a = (int)indices.getInt(_x++);
                    int b = (int)indices.getInt(_x++);
                    int c = (int)indices.getInt(_x);
                    m.faces[x] = new Face(a, b, c);

                }
                //System.out.println(faces);
                meshes.add(m);
            }
        }
        return meshes.get(0);
    }
}


