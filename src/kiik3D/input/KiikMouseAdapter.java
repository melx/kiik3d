package kiik3D.input;

import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;


public class KiikMouseAdapter extends MouseAdapter {
    @Override
    public void mouseClicked(MouseEvent e) {
        //System.out.println("Mouse clicked: "+e.getPoint()+", "+e.getID());
    }

    @Override
    public void mousePressed(MouseEvent e) {
        Input.setKey(e.getButton(), true);
        Input.setMousePressPos(e.getX(), e.getY());
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        Input.setKey(e.getButton(), false);
        Input.setMouseRelPos(e.getX(), e.getY());
    }
}
