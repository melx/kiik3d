package kiik3D.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.Point;

import java.util.HashMap;
import kiik3D.types.Vec2;


public class Input {
    private static HashMap<Integer, Boolean> input = new HashMap<Integer, Boolean>();
    private static InputDispatcher inputDispatcher = new InputDispatcher();
    private static KiikKeyAdapter keyAdapter = new KiikKeyAdapter();
    private static KiikMouseAdapter mouseAdapter = new KiikMouseAdapter();

    public static Vec2 mousePressCoord = new Vec2(0, 0);
    public static Vec2 mouseReleaseCoord = new Vec2(0, 0);

    public static void setKey(int c, boolean b) {
        input.put(c, b);
    }

    public static boolean getKey(int c) {
        if(!input.containsKey(c)) {
            return false;
        }
        return input.get(c);
    }

    public static InputDispatcher getDispatcher() {
        return inputDispatcher;
    }

    public static KeyAdapter getGLKeyAdapter() {
        return keyAdapter;
    }

    public static MouseAdapter getGLMouseAdapter() {
        return mouseAdapter;
    }

    public static void setMousePressPos(double x, double y) {
        mousePressCoord = new Vec2(x, y);
    }

    public static void setMouseRelPos(double x, double y) {
        mouseReleaseCoord = new Vec2(x, y);
    }
}
