package kiik3D.input;

import java.awt.event.KeyEvent;
import java.awt.KeyEventDispatcher;


class InputDispatcher implements KeyEventDispatcher {
    public boolean dispatchKeyEvent(KeyEvent e) {
        if (e.getID() == KeyEvent.KEY_PRESSED) {
            Input.setKey(e.getKeyCode(), true);
            //System.out.println("PRESS"+e.getKeyCode());
        } else if (e.getID() == KeyEvent.KEY_RELEASED) {
            //System.out.println("RELEASE"+System.currentTimeMillis());
            Input.setKey(e.getKeyCode(), false);
        }
        /*else if (e.getID() == KeyEvent.KEY_TYPED) {
            System.out.println("TYPE"+System.currentTimeMillis());
        }*/
        return false;
    }
}
