package kiik3D.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyAdapter;


class KiikKeyAdapter extends KeyAdapter {

    @Override
    public void keyPressed(KeyEvent e) {
        Input.setKey(e.getKeyCode(), true);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        Input.setKey(e.getKeyCode(), false);
    }
};
