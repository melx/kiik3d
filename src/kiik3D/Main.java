package kiik3D;

import javax.swing.SwingUtilities;

import org.json.JSONException;

import java.io.File;
import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;

import kiik3D.core.LegacySceneView;


public class Main {
    static DemoApp window;
    public static LegacySceneView legacySceneView;

    //Seadistatakse funktsioon, mida java graafilise liidese lõim jooksutab (createAppInstance)
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                try {
					createAppInstance();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        });
    }

    private static void createAppInstance() throws JSONException {
        Main.window = new DemoApp();
    }

    private static void listDir(String dir) {
        File[] files = (new File(dir)).listFiles();

        for(int i=0; i<files.length; i++){
            //Check if directory
            if(files[i].isDirectory())
                //Recursively call file list function on the new directory
                //fileList(files[i].get);
                System.out.println(files[i].getAbsolutePath());
            else{
                //If not directory, print the file path
                System.out.println(files[i].getAbsolutePath());
            }
        }
    }

    private static String readFile(String fn) {
        File file = new File(fn);
        FileInputStream fis;
        byte[] data;
        String s = "";

        try {
            fis = new FileInputStream(file);
            data = new byte[(int)file.length()];
            try {
                fis.read(data);
            } catch (IOException exc) {
                System.out.println(exc);
                return null;
            }

            try {
                s = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException exc) {
                System.out.println(exc);
            }
            try {
                fis.close();
            } catch (IOException exc) {
                System.out.println(exc);
            }
        } catch (FileNotFoundException exc) {
            System.out.println(exc);
        }
        return s;
    }

    /*
    private static void parseBabylonModel(String file) {
        InputStream stream = null;

        try {
            stream = new FileInputStream(file);

            JsonParserFactory factory = JsonParserFactory.getInstance();
            JSONParser parser = factory.newJsonParser();

            parser.setValidating(false);

            Map jsonData = parser.parseJson(stream, "UTF-8");

            System.out.println("Done");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if(stream!=null) {
                try {
                    stream.close();
                } catch(IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
    */

    public static void getMethodList(Class cls) {
        PrintWriter writer = null;
        try {
             writer = new PrintWriter(cls.getName()+".txt", "UTF-8");
        } catch(Throwable ex) {
            ex.printStackTrace();
            return;
        }
        for(Method m : cls.getDeclaredMethods()) {
            writer.println(m);
        }
        writer.close();
        System.out.println("[Main.getMethodList]Generated method list for "+cls.getName());
    }
}
