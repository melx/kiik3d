package kiik3D;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.*;

import org.json.JSONException;

import kiik3D.core.ResourceManager;
import kiik3D.core.EntityManager;


//Stseenide kontroller
public class DemoApp {
    private JFrame window; //Java Akna handler
    private DemoScene scene; //e 3D ruumi kontroller

    public DemoApp() throws JSONException {
        this.init();
    }

    private void init() throws JSONException {
        this.window = new JFrame("kiik3D App");
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Akna avamine
        window.setSize(1000, 700);
        window.setVisible(true);

        //Applikatsiooni resursside halduse käivitus
        ResourceManager.init();
        EntityManager.init();

        // STSEENI LAADIMINE
        // Stseeni käivitamine
        scene = new DemoScene();
        scene.initScene();
        //Keskkonna laadimine
        scene.loadScene();



        Dimension prefSize = new Dimension(640, 550);
        //GLJPanel sceneCanvas = scene.getCanvas();
        JPanel sceneCanvas = scene.getCanvas();
        sceneCanvas.setPreferredSize(prefSize);


        JPanel entity_panel = new JPanel();
        prefSize = new Dimension(200, 200);
        entity_panel.setPreferredSize(prefSize);

        JPanel console_panel = new JPanel();
        prefSize = new Dimension(640, 60);
        console_panel.setPreferredSize(prefSize);

        JSplitPane left = new JSplitPane(JSplitPane.VERTICAL_SPLIT, sceneCanvas, console_panel);
        left.setDividerSize(2);
        JSplitPane container = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, left, entity_panel);
        container.setDividerSize(2);


        window.getContentPane().add(container, BorderLayout.CENTER);

        System.out.println("Application loaded.");
    }

    public DemoScene getScene() {
        return this.scene;
    }

}
