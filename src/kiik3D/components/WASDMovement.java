package kiik3D.components;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import kiik3D.core.Engine;
import kiik3D.core.SceneView;
import kiik3D.core.SceneScope;
import kiik3D.core.Perspective;

import kiik3D.objects.Object3D;

import kiik3D.input.Input;
import kiik3D.types.Vec3;

import kiik3D.core.SceneScope;


public class WASDMovement extends Component {
    private boolean mode_change = false;
    private boolean mouse1_down = true;

    public double moveSpeed = 10;
    public double rotationSpeed = 90;


    public void update(Object3D gO) {

        /*
        boolean mouse1 = Input.getKey(MouseEvent.BUTTON1);
        if(!mouse1_down && mouse1) {
            System.out.println("[DemoComponent]"+Input.mousePressCoord);
            mouse1_down = true;
            Perspective p = SceneScope.getPerspective();
            System.out.println(p);
        } else if(!mouse1) {
            mouse1_down = false;
        }
        */
        Engine e = SceneScope.getEngine();
        double speed, rotSpeed;
        double deltaTime = e.getDeltaTime();

        if(Input.getKey(KeyEvent.VK_SHIFT)) {
            speed = 2*moveSpeed*deltaTime;
            rotSpeed = 2*rotationSpeed*deltaTime;
        } else {
            speed = moveSpeed*deltaTime;
            rotSpeed = rotationSpeed*deltaTime;
        }

        if(!mode_change) {
            if(Input.getKey(KeyEvent.VK_R)) {
                e.render_type = (e.render_type + 1)  % e.RENDER_MODES;
                mode_change = true;
            }
        } else {
            if(!Input.getKey(KeyEvent.VK_R)) {
                mode_change = false;
            }
        }

        if(Input.getKey(KeyEvent.VK_CONTROL)) {
            if(Input.getKey(KeyEvent.VK_W)) {
                gO.Rotate(-rotSpeed, 0, 0);
            } else if (Input.getKey(KeyEvent.VK_S)) {
                gO.Rotate(rotSpeed, 0, 0);
            }
            if(Input.getKey(KeyEvent.VK_A)) {
                gO.Rotate(0, -rotSpeed, 0);
            } else if (Input.getKey(KeyEvent.VK_D)) {
                gO.Rotate(0, rotSpeed, 0);
            }
            /*
        } else if(Input.getKey(KeyEvent.VK_ALT)) {
            if(Input.getKey(KeyEvent.VK_A)) {
                gO.Scale(scaleSpeed, 0, 0);
            } else if (Input.getKey(KeyEvent.VK_D)) {
                gO.Scale(-scaleSpeed, 0, 0);
            }
            if(Input.getKey(KeyEvent.VK_W)) {
                gO.Scale(0, scaleSpeed, 0);
            } else if (Input.getKey(KeyEvent.VK_S)) {
                gO.Scale(0, -scaleSpeed, 0);
            }
            if(Input.getKey(KeyEvent.VK_Q)) {
                gO.Scale(0, 0, scaleSpeed);
            } else if (Input.getKey(KeyEvent.VK_E)) {
                gO.Scale(0, 0, -scaleSpeed);
            }
            */
        } else {
            if(Input.getKey(KeyEvent.VK_A)) {
                gO.Translate(-speed, 0, 0);
            } else if (Input.getKey(KeyEvent.VK_D)) {
                gO.Translate(speed, 0, 0);
            }

            if(Input.getKey(KeyEvent.VK_W)) {
                gO.Translate(0, 0, -speed);
            } else if (Input.getKey(KeyEvent.VK_S)) {
                gO.Translate(0, 0, speed);
            }
        }

    }
}
