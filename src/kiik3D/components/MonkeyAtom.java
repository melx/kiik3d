package kiik3D.components;

import java.lang.Math;
import kiik3D.core.SceneScope;
import kiik3D.core.EntityManager;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import kiik3D.core.Engine;
import kiik3D.core.SceneView;
import kiik3D.core.SceneScope;
import kiik3D.core.Perspective;

import kiik3D.objects.Object3D;
import kiik3D.objects.Entity;

import kiik3D.input.Input;
import kiik3D.types.Vec3;


public class MonkeyAtom extends Component {
	public int layers = 2;
	
	private int electron_count;
	private Entity[] electrons;
	private Vec3[] rot_data;

    public double thetaSpeed = 1.0;
	public double omegaSpeed = 0.6;
	
	public MonkeyAtom() {
		electron_count = (int)Math.pow(2, layers);
		electrons = new Entity[electron_count];
		rot_data = new Vec3[electron_count];
		
		Engine e = SceneScope.getEngine();
		System.out.println("MA;"+electron_count);
		
		int radius = 4;
		int current_layer = 0;
		for(int i=0; i<electron_count; i++) {
			
			int l = (int)Math.floor(i/2);
			if(  l > current_layer ) {
				current_layer = l;
				radius = radius*2;
			}
			
			Entity entity = EntityManager.instantiate("Suzanne");
			electrons[i] = entity;
			rot_data[i] = new Vec3(radius, getRandRotationVal(3.14), getRandRotationVal(3.14));
			
			e.spawnObject(entity);
		}
	}

    public void update(Object3D gO) {
        Engine e = SceneScope.getEngine();
        double deltaTime = e.getDeltaTime();

		double dThetaSpeed = thetaSpeed*deltaTime;
		double dOmegaSpeed = omegaSpeed*deltaTime;
		
		for(int i=0; i < electron_count; i++) {
			Vec3 r_data = rot_data[i];
			r_data.Y += dThetaSpeed+getRandRotationVal(0.5)*deltaTime;
			r_data.Z += dOmegaSpeed+getRandRotationVal(0.5)*deltaTime;
			rot_data[i] = r_data;
			
			Vec3 pos = getElectronOffset(r_data);
			electrons[i] .setPosition(pos.X, pos.Y, pos.Z);
			
		}
     
    }
	
	Vec3 getElectronOffset(Vec3 rot_data) {
		double theta = rot_data.Y;
		double gamma = rot_data.Z;
		double r = rot_data.X;

		return new Vec3(r*Math.sin(theta)*Math.cos(gamma), r*Math.sin(theta) *Math.sin(gamma), r*Math.cos(theta));
	}
	
	double getRandRotationVal(double mx) {
		return (Math.random() * (mx*2))-mx;
	}
}
