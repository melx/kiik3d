package kiik3D.components;

import kiik3D.core.Engine;
import kiik3D.objects.Object3D;


public class Component implements java.io.Serializable {
    public void update(Object3D gameObject){};
    public String getID() {
        return this.getClass().getName();
    };

}
