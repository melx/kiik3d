package kiik3D.types;


public class Material {
    private Shader shader;

    private String name;
    private String ID;
    private String diffuseTexture;

    public String getName() { return name; }
    public void setName(String value) { name = value; }

    public String getID() { return ID; }
    public void setID(String value) { ID = value; }

    public String getDiffuseTexture() { return diffuseTexture; }
    public void setDiffuseTexture(String value) { diffuseTexture = value; }

    public Material(String ID, String name, String diffuseTexture) {
        this.name = name;
        this.ID = ID;
        this.diffuseTexture = diffuseTexture;
    }

    public Shader getShader() {
        return shader;
    }
}
