package kiik3D.types;

public class Matrix implements java.io.Serializable {
    protected double[] values;
    protected int columns;
    protected int rows;

    public Matrix() {

    }

    public Matrix(int columns, int rows) {
        this.columns = columns;
        this.rows = rows;
    }

    public Matrix(int columns, double[] values) {
        this.values = values;
        this.columns = columns;
        this.rows = values.length / columns;
    }


    public int getCols() { return columns; }
    public int getRows() { return rows; }
    public int getLength() { return values.length; }

    public double[] getValues() { return values; }
    public void setValues(double[] values) { this.values = values; }

    public void setColumn(int column, double[] columnValues) {
        int column_index = column * rows;
        for(int i = 0; i < columnValues.length; i++) {
            values[column_index++] = columnValues[i];
        }
    }

    public void setElem(int c, int r, double val) { values[ c*4 + r ] = val; }
    public double getElem(int c, int r) { return values[ c*4 + r ]; }
    public double get(int i) { return values[i]; }

    public double[] getColumn(int c) {
        int column_index = c*rows;
        double[] output = new double[rows];
        for(int i=0; i<rows;i++) {
            output[i] = values[column_index++];
        }
        return output;
    }

    public void setDimensions(int columns, int rows) { this.columns = columns; this.rows = rows; }

    public Matrix add(Matrix a) { //FIXME: update for new matrix syntax
        Matrix output = new Matrix();

        if(a.getLength()<values.length) {
            output.setDimensions(a.getCols(), a.getRows());
            for(int i = 0; i < a.getLength(); i++)
            {
                double r = a.getElem(i,0) + this.getElem(i, 0);
                output.setElem(i, 0, r);
            }
        } else {
            output.setDimensions(getCols(), getRows());
            for(int i = 0; i < values.length; i++)
            {
                double r = a.getElem(i,0) + this.getElem(i, 0);
                output.setElem(i, 0, r);
            }
        }

        return output;
    }

    public void addToSelf(Matrix a) { //FIXME: update for new matrix syntax
        if(a.getLength()<values.length) {
            for(int i = 0; i < a.getLength(); i++)
            {
                double r = a.getElem(i,0) + this.getElem(i, 0);
                this.setElem(i, 0, r);
            }
        } else {
            for(int i = 0; i < values.length; i++)
            {
                double r = a.getElem(i,0) + this.getElem(i, 0);
                this.setElem(i, 0, r);
            }
        }
    }

    public Matrix subtract(Matrix a) { //FIXME: update for new matrix syntax
        Matrix output = new Matrix();

        if(a.getLength()<values.length) {
            output.setDimensions(a.getCols(), a.getRows());
            for(int i = 0; i < a.getLength(); i++)
            {
                double r = a.getElem(i,0) - this.getElem(i, 0);
                output.setElem(i, 0, r);
            }
        } else {
            output.setDimensions(getCols(), getRows());
            for(int i = 0; i < values.length; i++)
            {
                double r = a.getElem(i,0) - this.getElem(i, 0);
                output.setElem(i, 0, r);
            }
        }

        return output;
    }


    public Matrix multiply(Matrix input) {
        if(getCols()!=input.getRows()) {
            System.out.println("[Matrix.multiply(Matrix)]ERROR: Invalid matrix sizes");
            return null;
        }

        double[] output_values = new double[input.getCols() * getRows()];
        for(int i = 0; i < output_values.length; i++) {
            output_values[i] = 0;
        }

        for(int input_column=0; input_column < input.getCols(); input_column++ ) {
            double[] input_vec = input.getColumn(input_column);

            for(int column = 0; column < columns; column++) {
                double output_elem = 0;
                double input_elem = input_vec[column];
                for(int row=0; row < rows; row++) {
                    int elem_index = column * rows + row;
                    output_values[input_column * rows + row] += input_elem * values[elem_index];

                }
            }

        }
        Matrix output = new Matrix(input.getCols(), output_values);

        return output;
    }

    public Matrix4 multiply(Matrix4 input) {
        if(getCols()!=input.getRows()) {
            System.out.println("[Matrix.multiply(Matrix)]ERROR: Invalid matrix sizes");
            return null;
        }

        double[] output_values = new double[input.getCols() * getRows()];
        for(int i = 0; i < output_values.length; i++) {
            output_values[i] = 0;
        }

        for(int input_column=0; input_column < input.getCols(); input_column++ ) {
            double[] input_vec = input.getColumn(input_column);

            for(int column = 0; column < columns; column++) {
                double output_elem = 0;
                double input_elem = input_vec[column];
                for(int row=0; row < rows; row++) {
                    int elem_index = column * rows + row;
                    output_values[input_column * rows + row] += input_elem * values[elem_index];

                }
            }

        }
        Matrix4 output = new Matrix4(output_values);

        return output;
    }

    public double[] multiply(double x, double y, double z) { //FIXME: update for new matrix syntax
        double[] result = new double[]{0, 0, 0};

        for(int row=0; row < 3; row++) {
            int elem_index = row * columns;
            result[row] = x * values[elem_index++] + y * values[elem_index++] + z * values[elem_index++] + values[elem_index++];
        }
        System.out.println("TODO:Matrix.multiply(double, double, double)");

        return result;
    }

    public double[] multiply(double x, double y, double z, double w) {
        double[] result = new double[]{0, 0, 0, 0};

        for(int c = 0; c < 4; c++) {
            int elem_index = c * rows;
            result[0] += x * values[elem_index++];
            result[1] += y * values[elem_index++];
            result[2] += z * values[elem_index++];
            result[3] += w * values[elem_index++];
        }

        return result;
    }

    public Vec4 multiply(Vec4 v) {
        double[] result = multiply(v.X, v.Y, v.Z, v.W);
        Vec4 output = v.createNew(result);
        return output;
    }

    public Vec3 multiply(Vec3 v) {
        double[] result = multiply(v.X, v.Y, v.Z, 1);
        Vec3 output = v.createNew(result);
        return output;
    }


    public Matrix divide(Matrix m) {
        //TODO

        Matrix r = new Matrix();
        return r;
    }
}
