package kiik3D.types;


//3D Mudeli klass
public class Mesh implements java.io.Serializable {
    private static int id_seq=0;

    private String name;
    public Vertex[] vertices;
    public Face[] faces;
    private Vertex[] transformedVertices;


    public Mesh(String name, int verticesCount, int faceCount) {
        name = name;
        vertices = new Vertex[verticesCount];
        faces = new Face[faceCount];
    }

    public Mesh() {

    }

    public Mesh(int verticesCount, int faceCount) {
        name = "Mesh"+(++id_seq);
        vertices = new Vertex[verticesCount];
        faces = new Face[faceCount];
    }

    public Mesh(String name, Vertex[] vertices, Face[] faces) {
        this.name = name;
        this.vertices = vertices;
        this.faces = faces;
    }

    public void setVertices(Vertex[] _vertices) {
        vertices = _vertices;
    }

    public Face[] getFaces() {
        return faces;
    }

    public void setFaces(Face[] _faces) {
        faces = _faces;
    }


    // Rendering methods
    public void transform(Matrix4 modelMatrix) {
        transformedVertices = new Vertex[vertices.length];
        for(int i=0;i<vertices.length;i++) {
            Vertex vert = modelMatrix.Project(vertices[i]);
            transformedVertices[i] = vert;
        }
    }

    public Vertex[] getTransformedVertices() {
        return transformedVertices;
    }

}
