package kiik3D.types;

import java.lang.Math;


public class Vec3 extends Vec {
    public double X;
    public double Y;
    public double Z;
    public Vec2 TextureCoordinates = null;

    public Vec3(double x, double y, double z) {
        X = x;
        Y = y;
        Z = z;
    }

    public Vec3(double[] values) {
        X = values[0];
        Y = values[1];
        Z = values[2];
    }

    public Vec3 createNew(double[] values) {
        return new Vec3(values[0], values[1], values[2]);
    }

    public double get(int i) {
        switch(i) {
            case 0: return X;
            case 1: return Y;
            case 2: return Z;
            default: return 0.0;
        }
    }

    public double getX() {
        return X;
    }

    public double getY() {
        return Y;
    }

    public double getZ() {
        return Z;
    }

    public Vec3 add(Vec3 v) {
        return new Vec3(X + v.X, Y + v.Y, Z + v.Z);
    }

    public Vec3 subtract(Vec3 v) {
        return new Vec3(X - v.X, Y - v.Y, Z - v.Z);
    }

    public Vec3 subAndAssign(Vec3 a, Vec3 b) {
        X = a.X - b.X;
        Y = a.Y - b.Y;
        Z = a.Z - b.Z;

        return this;
    }

    public double l2Norm() {
        return (double) Math.sqrt(X*X+Y*Y+Z*Z);
    }

    public Vec3 crossAndAssign(Vec3 a, Vec3 b) {
        double tempX = a.Y * b.Z - a.Z * b.Y;
        double tempY = a.Y * b.X - a.X * b.Z;
        double tempZ = a.X * b.Y - a.Y * b.X;

        X = tempX;
        Y = tempY;
        Z = tempZ;

        return this;
    }

    public Vec3 scale(double scalar) {
        X *= scalar;
        Y *= scalar;
        Z *= scalar;

        return this;
    }

    public Vec3 normalize() {
        double length = l2Norm();
        X /= length;
        Y /= length;
        Z /= length;

        return this;
    }

    public String toString() {
        return "<Vec3 ( "+X+", "+Y+", "+Z+")>";
    }
}
