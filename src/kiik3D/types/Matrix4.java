package kiik3D.types;


//http://www.cs.princeton.edu/~gewang/projects/darth/stuff/quat_faq.html#Q26
// Using openGL format
//| 1 5  9 13 |
//| 2 6 10 14 |
//| 3 7 11 15 |
//| 4 8 12 16 |
//
public class Matrix4 extends Matrix  {
    public Matrix4() {
        columns = 4;
        rows = 4;
        values = new double[16];
    }

    public Matrix4(double[] values) {
        if(values.length!=16) {
            System.out.println("INVALID Matrix4 CONSTRUCTOR ARGUMENT"); //TODO: handle properly
        }
        this.values = values;
        columns = 4;
        rows = 4;
    }

    public double[] Transpose() {
        double[] output = new double[values.length];
        for(int i=0; i < 4 ;i++) {
            for(int e=0; e < 4 ;e++) {
                output[ i*4 + e] = values[ (e*4) + i ];
            }
        }
        return output;
    }

    public void Translate(Vec3 offset) {
        values[12] += offset.X;
        values[13] += offset.Y;
        values[14] += offset.Z;
    }

    public void Translate(double X, double Y, double Z) {
        values[12] += X;
        values[13] += Y;
        values[14] += Z;
    }

    public void Scale(Vec3 offset) {
        values[12] *= offset.X;
        values[13] *= offset.Y;
        values[14] *= offset.Z;
    }

    public void Scale(double sX, double sY, double sZ) {
        values[12] *= sX;
        values[13] *= sY;
        values[14] *= sZ;
    }

    public void Rotate(double aX, double aY, double aZ) {
        values = rotationMatrix(aX, aY, aZ).multiply(this).getValues();
    }

    public Vertex Project(Vertex v) {
        double[] result = multiply(v.getX(), v.getY(), v.getZ());
        Vertex output = v.createNew(result);
        return output;
    }

    //
    // Identity
    //
    public static Matrix4 identityMatrix() {
        Matrix4 m = new Matrix4();
        m.toIdentity();
        return m;
    }

    public void toIdentity() {
        values[0] = 1;values[4] = 0;values[8] =  0;values[12] = 0;
        values[1] = 0;values[5] = 1;values[9] =  0;values[13] = 0;
        values[2] = 0;values[6] = 0;values[10] = 1;values[14] = 0;
        values[3] = 0;values[7] = 0;values[11] = 0;values[15] = 1;
    }


    //
    // Translation
    //
    public static Matrix4 translationMatrix(double dX, double dY, double dZ)
    {
        Matrix4 m = new Matrix4();
        m.toTranslation(dX, dY, dZ, 1);
        return m;
    }

    public void toTranslation(double x, double y, double z, double w) {
        values[0] = 1;values[4] = 0;values[8] =  0;values[12] = x;
        values[1] = 0;values[5] = 1;values[9] =  0;values[13] = y;
        values[2] = 0;values[6] = 0;values[10] = 1;values[14] = z;
        values[3] = 0;values[7] = 0;values[11] = 0;values[15] = w;
    }

    //
    // Scale
    //
    public static Matrix4 scaleMatrix(double x, double y, double z)
    {
        Matrix4 m = new Matrix4();
        m.toScale(x, y, z);
        return m;
    }

    public void toScale(double x, double y, double z) {
        values[0] = x;values[4] = 0;values[8] =  0;values[12] = 0;
        values[1] = 0;values[5] = y;values[9] =  0;values[13] = 0;
        values[2] = 0;values[6] = 0;values[10] = z;values[14] = 0;
        values[3] = 0;values[7] = 0;values[11] = 0;values[15] = 1;
    }

    //
    //Rotation
    //
    public static Matrix4 rotationMatrix(double X, double Y, double Z) {
        Matrix4 m = new Matrix4();
        m.toRotation(X, Y, Z);
        return m;
    }

    public void toRotX(double A) {
        toIdentity();
        int column = columns;
        values[++column] = Math.cos(A);
        values[++column] = -Math.sin(A);
        column = 2*columns;
        values[++column] = Math.sin(A);
        values[++column] = Math.cos(A);
    }

    public void toRotY(double A) {
        toIdentity();
        int column = columns;
        values[0] = Math.cos(A);
        values[2] = Math.sin(A);
        column = 2*columns;
        values[column] = -Math.sin(A);
        values[column+2] = Math.cos(A);
    }

    public void toRotZ(double A) {
        toIdentity();
        int column = columns;
        values[0] = Math.cos(A);
        values[1] = -Math.sin(A);
        column = columns;
        values[column] = Math.sin(A);
        values[++column] = Math.cos(A);
    }

    public void toRotation(double aX, double aY, double aZ) {
        toIdentity();
        double A = Math.cos(aX);
        double B = Math.sin(aX);
        double C = Math.cos(aY);
        double D = Math.sin(aY);
        double E = Math.cos(aZ);
        double F = Math.sin(aZ);

        double AD = Math.cos(aX) * Math.sin(aY);
        double BD = Math.sin(aX) * Math.sin(aY);

        //0 4  8 12
        //1 5  9 13
        //2 6 10 14
        //3 7 11 15
        //
        //0 1 2 3
        //4 5 6 7
        //8 9 10 11
        //12 13 14 15
        //
        values[0]  =   C * E;
        values[4]  =  -C * F;
        values[8]  =   D;

        values[1]  =  BD * E + A * F;
        values[5]  = -BD * F + A * E;
        values[9]  =  -B * C;

        values[2]  = -AD * E + B * F;
        values[6]  =  AD * F + B * E;
        values[10] =   A * C;

        values[3] = values[7] = values[11] = values[12] = values[13] = values[14] = 0;

        values[15] = 1;
    }


    //
    // ----------- MISC ------------
    //

    public String toString() {
        String output = "<Matrix4 ";

        int row, rows = getRows(), cols = getCols();
        int pre_mx_col = cols-1;

        for(int r=0;r<rows;r++) {
            row = r*cols;

            output += " [ ";
            for(int c=0;c<cols;c++) {
                output += values[row+c];
                if(c<pre_mx_col) {
                    output += ", ";
                } else {
                    output += " ";
                }
            }
            output += "]";
        }
        output += ">";
        return output;
    }


}
