package kiik3D.types;


public class Vec4 extends Vec {
    public double X;
    public double Y;
    public double Z;
    public double W;


    public Vec4(double x, double y, double z) {
        X = x;
        Y = y;
        Z = z;
        W = 0;
    }

    public Vec4(double x, double y, double z, double w) {
        X = x;
        Y = y;
        Z = z;
        W = w;
    }

    public Vec4(double[] values) {
        X = values[0];
        Y = values[1];
        Z = values[2];
        W = values[3];
    }

    public Vec4 createNew(double[] values) {
        return new Vec4(values[0], values[1], values[2], values[3]);
    }

    public double get(int i) { //TODO: remove this method
        double[] values = new double[]{X, Y, Z, W};
        if(i < values.length) return values[i];
        return 0.0;
    }

    public double getX() {
        return X;
    }

    public double getY() {
        return Y;
    }

    public double getZ() {
        return Z;
    }

    public double getW() {
        return W;
    }

    //FIXME: W coordinate handling
    public Vec4 add(Vec4 v) {
        return new Vec4(X + v.X, Y + v.Y, Z + v.Z);
    }

    public Vec4 subtract(Vec4 v) {
        return new Vec4(X - v.X, Y - v.Y, Z - v.Z);
    }

    public String toString() {
        return "<Vec4 ( "+X+", "+Y+", "+Z+", "+W+")>";
    }
}
