package kiik3D.types;

public abstract class Vec<T> implements java.io.Serializable {
    public abstract Vec createNew(double[] values);
}
