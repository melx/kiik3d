package kiik3D.types;

public class Vertex implements java.io.Serializable {
    private Vec3 position;
    private Vec3 normal = new Vec3(0, 0, 0);
    private Vec2 texCoords = new Vec2(0, 0);

    public Vertex(Vec3 position) {
        this.position = position;
    }

    public Vertex(double x, double y, double z) {
        position = new Vec3(x, y, z);
    }

    public Vertex createNew(double[] values) {
        Vertex v = new Vertex(values[0], values[1], values[2]);
        v.setUV(this.texCoords);
        return v;
    }

    public Vec3 getPosition() { return position; }

    public double getX() { return position.X; }
    public double getY() { return position.Y; }
    public double getZ() { return position.Z; }

    public void setX(double value) { position.X = value; }
    public void setY(double value) { position.Y = value; }
    public void setZ(double value) { position.Z = value; }

    public Vec3 getNormal() { return normal; }
    public void setNormal(Vec3 value) { normal = value; }
    public void setNormal(double x, double y, double z) { normal.X=x;normal.Y=y;normal.Z=z; }

    public Vec2 getUV() { return texCoords; }
    public void setUV(double u, double v) { texCoords.X = u;texCoords.Y = v; }
    public void setUV(Vec2 uvCoords) { texCoords.X = uvCoords.X;texCoords.Y = uvCoords.Y; }


    public String toString() {
        return "<Vertex ( "+position.X+", "+position.Y+", "+position.Z+"; ["+texCoords.X+", "+texCoords.Y+"])>";
    }
}
