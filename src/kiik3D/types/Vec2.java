package kiik3D.types;

public class Vec2 extends Vec {
    public double X;
    public double Y;

    public Vec2(double x, double y) {
        X = x;
        Y = y;
    }

    public Vec2(double[] values) {
        X = values[0];
        Y = values[1];
    }

    public Vec2 createNew(double[] values) {
        return new Vec2(values[0], values[1]);
    }

    public double getX() {
        return X;
    }

    public double getY() {
        return Y;
    }

    public Vec2 add(Vec2 v) {
        return new Vec2(X+v.getX(), Y+v.getY());
    }

    public Vec2 subtract(Vec2 v) {
        return new Vec2(X-v.getX(), Y-v.getY());
    }

    public String toString() {
        return "<Vec2 ( "+X+", "+Y+")>";
    }
}
