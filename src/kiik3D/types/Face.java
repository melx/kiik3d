package kiik3D.types;

public class Face implements java.io.Serializable {
    public int A;
    public int B;
    public int C;

    public Face(int a, int b, int c) {
        A=a;
        B=b;
        C=c;
    }

    public Vertex[] getVertices(Vertex[] verts) {
        return new Vertex[]{verts[A], verts[B], verts[C]};
    }
}
