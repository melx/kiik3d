package kiik3D.core;

import static javax.media.opengl.GL.*;
import static javax.media.opengl.GL2.*;

import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;

import javax.media.opengl.GLProfile;
import javax.media.opengl.GLCapabilities;

//import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.awt.GLJPanel; //Java aknas olev 2D pind, millel tegevus toimub

import com.jogamp.opengl.util.*;

import kiik3D.core.SceneContext;
import kiik3D.core.SceneScope;
import kiik3D.core.Perspective;
import kiik3D.input.Input;


//3D keskkonna klass
public abstract class SceneView implements GLEventListener {
    protected Engine engine;  //Objekt, kus toimuvad 3D ruumis olevate kehade liigutamine ning nende 2D tasandile projekteerimine
    protected Perspective persp = new Perspective();
    protected boolean running = true;
    private boolean _configured = false;

    // GL Variables
    protected GLProfile glprofile;
    protected GLCapabilities glcapabilities;
    //protected final GLCanvas glcanvas;
    protected GLJPanel glcanvas;
    protected Animator animator;
    protected GLU glu;


    //Physics clock variables
    private double fps;
    private double fpsTimer;
    private int fpsCounter;

    private int fpsLimit = 60;
    private double dTLim = 1.0/(fpsLimit+1);
    private static double frameTime = System.currentTimeMillis();
    public static double deltaTime;

    private double dT;


    public SceneView() {
        engine = new Engine();

        dT = 0;
        fps = 0;
        fpsTimer = System.currentTimeMillis();
        fpsCounter = 0;
    }

    public void initScene() {
        initGL();
    }

    public GLJPanel getCanvas() {
        return glcanvas;
    }

    public void initGL() {
        if(_configured) {
            System.out.println("[SceneView]initGL() call failed, Scene is already configured");
            return;
        }

        glprofile = GLProfile.getDefault();
        glcapabilities = new GLCapabilities(glprofile);

        //glcanvas = new GLCanvas( glcapabilities );
        glcanvas = new GLJPanel(glcapabilities);

        glcanvas.addGLEventListener(this); //Käivita opengl funktsionaalsus SceneView'd kasutades
        initGLInput(glcanvas);

        animator = new Animator(glcanvas);
        animator.start();
        _configured = true;

        SceneContext context = new SceneContext(engine, this);
        SceneScope.set(context);
    }

    public void initGLInput(GLJPanel canvas) {
        canvas.addKeyListener(Input.getGLKeyAdapter());
        canvas.addMouseListener(Input.getGLMouseAdapter());
    }

    public void reshape( GLAutoDrawable glautodrawable, int x, int y, int width, int height ) {
        GL2 gl2 = glautodrawable.getGL().getGL2();

        if (height == 0) height = 1;
        persp.setSize(width, height);

        gl2.glViewport(0, 0, width, height);

        gl2.glMatrixMode(GL2.GL_PROJECTION);
        gl2.glLoadIdentity();
        glu = new GLU();
        persp.set(glu);

        gl2.glMatrixMode(GL2.GL_MODELVIEW);
        gl2.glLoadIdentity();
    }

    public void init( GLAutoDrawable glautodrawable ) {
        System.out.println("GLInit");
        GL2 gl = glautodrawable.getGL().getGL2();
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // taustavärvuse seadistus
        gl.glClearDepth(1.0f);      // tausta kaugus seadistatakse maksimumile
        gl.glEnable(GL_DEPTH_TEST); // sügavuspuhvri käivitus
        gl.glDepthFunc(GL_LEQUAL);  // sügavustesti tüüp
        gl.glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // kasuta parimat perspektiivi korrastust
        gl.glShadeModel(GL_SMOOTH); // sile valgustus ja värvide üleminek
        glautodrawable.getGL().setSwapInterval(1);

        //gl.glPolygonMode(GL_FRONT, GL_LINE);
        //gl.glPolygonMode(GL_BACK, GL_LINE);
        //gl.glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
        //glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
        /*
        int[] tmp = new int[];
        gl.glGenTextures(1, tmp);
        System.out.println();
        */

        ResourceManager.loadTextures();
    }

    public void dispose( GLAutoDrawable glautodrawable ) {
    }

    public void display( GLAutoDrawable glautodrawable ) {
        render( glautodrawable.getGL().getGL2(), glautodrawable.getSurfaceWidth(), glautodrawable.getSurfaceHeight() );
    }

    private void render(GL2 gl2, int renderWidth, int renderHeight) {
        deltaTime = (System.currentTimeMillis() - frameTime)/1000;

        if(deltaTime>=dTLim) {
            engine.update();

            frameTime = System.currentTimeMillis();
            engine.render(gl2);
            if(frameTime - fpsTimer > 1000) {
                fps = fpsCounter;
                //fps = (int)(fps * 0.5 + fpsCounter*0.5);
                //dT = deltaTime * 0.8 + dT * 0.2;

                fpsTimer = System.currentTimeMillis();
                fpsCounter = 0;
            } else {
                fpsCounter++;
            }
        }

    }

    public Perspective getPerspective() { return persp; }

    public abstract void loadScene();
}
