package kiik3D.core;

//Legacy imports
import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.Color;
import java.awt.AlphaComposite;

//openGL imports
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;

import java.util.ArrayList;
import java.lang.Math;

import kiik3D.objects.Entity;
import kiik3D.objects.Camera;

import kiik3D.types.Vec2;
import kiik3D.types.Vec3;
import kiik3D.types.Vec4;
import kiik3D.types.Matrix;
import kiik3D.types.Vertex;



public class Engine {
    private int renderWidth, renderHeight;
    private ArrayList<Entity> objects = new ArrayList<Entity>(); //3D ruumis eksisteerivate Entity objektide nimistu
    private ArrayList<Camera> cameras = new ArrayList<Camera>(1);
    private Camera mainCamera;

    //Legacy Variables
    private BufferedImage backBuffer; //Siia salvestatakse projekteeritud pilt
    private double[] depthBuffer;
    private Graphics2D g;
    private LegacySceneView legacyScene = null;

    private int test = 0;
    public int render_type = 2;
    public static final int RENDER_MODES = 3;
    // 0 - rasterization
    // 1 - swing poly raster
    // 2 - wireframe

    //Physics clock variables
    private static double frameTime = System.currentTimeMillis();
    public static double deltaTime = 1;


    public Engine() {
        renderWidth = 640;
        renderHeight = 480;
    }

    public Engine(LegacySceneView legacyScene) {
        renderWidth = 640;
        renderHeight = 480;
        initLegacy();
        this.legacyScene = legacyScene;
    }

    private void initLegacy() {
        backBuffer = new BufferedImage(renderWidth, renderHeight, BufferedImage.TYPE_INT_RGB);
        g = backBuffer.createGraphics();
        System.out.println("BACKBUFFER GRAPHICS: "+g);
        resetDepthBuffer();
    }

    public double getDeltaTime() {
        return deltaTime;
    }

    //3D ruumis olevate objektide liigutamine
    public void update() {
        deltaTime = (System.currentTimeMillis() - frameTime)/1000;
        frameTime = System.currentTimeMillis();
        for(Camera cam : cameras) {
            if(cam!=null) cam.update(this);
        }

        for(Entity o : objects) {
            if(o!=null) o.update(this);
        }
    }

    public void spawnObject(Entity o) {
        objects.add(o);
    }

    public void removeObject(Entity o) {
        if(objects.contains(o)) {
            objects.remove(o);
        }
    }

    //
    // openGL rendering
    //
    public void render(GL2 gl2) {
        GLU glu = GLU.createGLU(gl2);

        gl2.glClear(GL.GL_COLOR_BUFFER_BIT);

        gl2.glLoadIdentity();
        mainCamera.transform(gl2, glu);
        gl2.glPushMatrix();

        gl2.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
        for(Entity o : objects) {
            if(o!=null) o.render(gl2, this);
            gl2.glPopMatrix();
        }
    }

    //
    // Legacy rendering
    //
    public void legacyClear() {
        g.setComposite(AlphaComposite.Clear);
        g.fillRect(0, 0, renderWidth, renderHeight);
        g.setComposite(AlphaComposite.SrcOver);
    }

    private void resetDepthBuffer() {
        depthBuffer = new double[ renderWidth * renderHeight];
        for(int i=0;i<depthBuffer.length;i++) {
            depthBuffer[i] = Double.MAX_VALUE;
        }
    }

    public Graphics2D getGraphics() {
        return g;
    }

    public static double Clamp(double value, double mn, double mx) {
        return Math.max(mn, Math.min(value, mx));
    }

    public static double Clamp(double value) {
        return Math.max(0, Math.min(value, 1));
    }

    public static double Interpolate(double mn, double mx, double gradient) {
        return mn + (mx - mn) * Clamp(gradient);
    }

    public void putPixel(int x, int y, double z) {
        int i = x + y * renderWidth;

        if(depthBuffer[i] < z) {
            return;
        }

        depthBuffer[i] = z;
        g.draw(new Line2D.Double(x, y, x, y));
    }

    public void drawLine(Vertex v1, Vertex v2, Color c) {
        g.setColor(c);
        g.draw(new Line2D.Double(v1.getX(), v1.getY(), v2.getX(), v2.getY()));
    }

    public void drawLine(Vertex v1, Vertex v2) {
        g.draw(new Line2D.Double(v1.getX(), v1.getY(), v2.getX(), v2.getY()));
    }

    public void drawWTriangle(Vertex[] verts) {
        for(Vertex v1 : verts) {
            for(Vertex v2 : verts) {
                if(v1==v2) continue;
                drawLine(v1, v2);
            }
        }
    }

    public void drawFTriangle(Vertex[] verts) {
        int nPoints = verts.length;
        int[] xPoints = new int[nPoints];
        int[] yPoints = new int[nPoints];

        for(int i=0;i<nPoints;i++) {
            Vertex v = verts[i];
            xPoints[i] = (int)v.getX();
            yPoints[i] = (int)v.getY();
        }

        g.fillPolygon(xPoints, yPoints, nPoints);
    }

    public void DrawPoint(Vec3 p, Color color) {
        //FIXME: Check backBuffer.pixelWidth/Height instead
        g.setColor(color);
        if(p.getX() >= 0 && p.getY() >= 0 && p.getX() < renderWidth && p.getY() < renderHeight) {
            putPixel((int)p.X, (int)p.Y, p.Z);
        }
    }

    public void ProcessScanLine(int y, Vec3 pa, Vec3 pb, Vec3 pc, Vec3 pd, Color color)
    {
        double gradient1 = pa.Y != pb.Y ? (y - pa.Y) / (pb.Y - pa.Y) : 1;
        double gradient2 = pc.Y != pd.Y ? (y - pc.Y) / (pd.Y - pc.Y) : 1;

        int sx = (int)Interpolate(pa.X, pb.X, gradient1);
        int ex = (int)Interpolate(pc.X, pd.X, gradient2);

        double z1 = Interpolate(pa.Z, pb.Z, gradient1);
        double z2 = Interpolate(pc.Z, pd.Z, gradient2);

        for(int x = sx; x < ex; x++) {
            double gradient = (x - sx) / (double)(ex - sx);
            double z = Interpolate(z1, z2, gradient);
            DrawPoint(new Vec3(x, y, z), color);
        }
    }


    public void drawTriangle(Vertex v1, Vertex v2, Vertex v3, Color color)
    {
        Vec3 _p1 = v1.getPosition();
        Vec3 _p2 = v2.getPosition();
        Vec3 _p3 = v3.getPosition();

        Vec3 p1 = null, p2 = null, p3 = null;
        if (_p1.Y > _p2.Y)
        {
            p2 = _p1;
            p1 = _p2;
        }

        if (_p2.Y > _p3.Y)
        {
            p2 = _p3;
            p3 = _p2;
        }

        if (_p1.Y > _p2.Y)
        {
            p2 = _p1;
            p1 = _p2;
        }

        if(p1==null) p1 = _p1;
        if(p2==null) p2 = _p2;
        if(p3==null) p3 = _p3;

        double dP1P2, dP1P3;

        if (p2.Y - p1.Y > 0)
            dP1P2 = (p2.X - p1.X) / (p2.Y - p1.Y);
        else
            dP1P2 = 0;

        if (p3.Y - p1.Y > 0)
            dP1P3 = (p3.X - p1.X) / (p3.Y - p1.Y);
        else
            dP1P3 = 0;


        // P1
        // -
        // --
        // - -
        // -  -
        // -   - P2
        // -  -
        // - -
        // -
        // P3
        if (dP1P2 > dP1P3)
        {
            for (int y = (int)p1.Y; y <= (int)p3.Y; y++)
            {
                if (y < p2.Y)
                {
                    ProcessScanLine(y, p1, p3, p1, p2, color);
                }
                else
                {
                    ProcessScanLine(y, p1, p3, p2, p3, color);
                }
            }
        }
        //       P1
        //        -
        //       --
        //      - -
        //     -  -
        // P2 -   -
        //     -  -
        //      - -
        //        -
        //       P3
        else
        {
            for (int y = (int)p1.Y; y <= (int)p3.Y; y++)
            {
                if (y < p2.Y)
                {
                    ProcessScanLine(y, p1, p2, p1, p3, color);
                }
                else
                {
                    ProcessScanLine(y, p2, p3, p1, p3, color);
                }
            }
        }
    }

    public void drawFace(Vertex[] verts, Color c) {
        g.setColor(c);
        //drawTriangle(verts[0], verts[1], verts[2], c);
        switch(render_type) {
            case 0:
                drawTriangle(verts[0], verts[1], verts[2], c);
                break;
            case 1:
                drawFTriangle(verts);
                break;
            case 2:
                drawWTriangle(verts);

        }
    }

    public void legacyRender() {
        legacyClear();
        for(Entity o : objects) {
            if(o!=null) o.legacyRender(this);
        }
    }

    public BufferedImage getBackBuffer() {
        return this.backBuffer;
    }


    //
    // -------------- TESTS -------------
    //

    /*
    public void testMatrixMultiplication() {
        Matrix m1 = new Matrix();
        Matrix m2 = new Matrix();
        m1.setMatrix(4, new double[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16});
        m2.setMatrix(2, new double[]{1, 2, 3, 4, 5, 6, 7, 8});
        System.out.println(m1.toString());
        System.out.println(m2.toString());
        System.out.println(m1.multiply(m2));
    }

    public void testMatrixTypes() {
        Matrix scaleM = Matrix.scaleMatrix(new double[]{1, 2, 3});
        Matrix translateM = Matrix.transMatrix(new Vec4(10, 0, -5));
        Matrix rotM = Matrix.rotMatrix(0, 0, 0);

        Vec4 orig_pos = new Vec4(10, -10, 10, 1);

        System.out.println("ScaleM: "+scaleM.toString());
        System.out.println("TransM: "+translateM.toString());
        System.out.println("RotM: "+rotM.toString());
        System.out.println(orig_pos.toString());
        System.out.println("---");
    }

    public void testMatrixAddition() {
        Matrix m1 = new Matrix();
        Matrix m2 = new Matrix();
        m1.setMatrix(4, new double[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16});
        m2.setMatrix(2, new double[]{1, 2, 3, 4, 5, 6, 7, 8});
        System.out.println(m1.toString());
        System.out.println(m2.toString());
        System.out.println(m1.add(m2));
    }

    public void testMatrixTranslation() {
        Matrix m1 = new Matrix();
        m1.setMatrix(4, new double[]{1, 0, 0, 10, 0, 1, 0, 20, 0, 0, 1, 30, 0, 0, 0, 1});
        m1.Translate(new Vec3(-20, -20, -20));
        System.out.println(m1);
    }
    */

    public void setMainCamera(Camera cam) {
        System.out.println("Addcam" +cam);
        cameras.add(cam);
        mainCamera = cam;
    }

    public Camera getMainCamera() {
        return mainCamera;
    }
}
