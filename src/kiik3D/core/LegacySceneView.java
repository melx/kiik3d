package kiik3D.core;

import javax.swing.JPanel;
import javax.swing.BorderFactory;

import java.awt.Toolkit;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.event.KeyEvent;

import kiik3D.input.Input;


//3D keskkonna klass
public abstract class LegacySceneView extends JPanel {
    protected Engine engine;  //Objekt, kus toimuvad 3D ruumis olevate kehade liigutamine ning nende 2D tasandile projekteerimine
    private Thread engineThread; //Lõim, milles jookseb 3D ruumi kontroller

    protected boolean running = true;
    private boolean _configured = false;

    //Physics clock variables
    private double fps;
    private double fpsTimer;
    private int fpsCounter;

    private int fpsLimit = 60;
    private double dTLim = 1.0/(fpsLimit+1);
    private static double frameTime = System.currentTimeMillis();
    public static double deltaTime;

    private double dT;

    //Constructor
    public LegacySceneView() {
        engine = new Engine(this);

        dT = 0;
        fps = 0;
        fpsTimer = System.currentTimeMillis();
        fpsCounter = 0;
    }

    public void initScene() {
        initLegacy();
        initLegacyInput();
    }

    public JPanel getCanvas() {
        return this;
    }

    public void initLegacy() {
        if(_configured) {
            System.out.println("[SceneView]initLegacy() call failed, Scene is already configured");
            return;
        }

        final LegacySceneView currentScene = this;
        Runnable r1 = new Runnable() {
            public void run() {
                while(currentScene.running) {
                    currentScene.legacyRender();
                }
            }
        };
        engineThread = new Thread(r1);
        engineThread.start();

        _configured = true;
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawString("RENDER_SCENE",10,20);
    }

    public void legacyRender() {
        deltaTime = (System.currentTimeMillis() - frameTime)/1000;

        if(deltaTime>=dTLim) {
            engine.update();

            frameTime = System.currentTimeMillis();
            engine.legacyRender();
            if(frameTime - fpsTimer > 1000) {
                fps = fpsCounter;
                //fps = (int)(fps * 0.5 + fpsCounter*0.5);
                //dT = deltaTime * 0.8 + dT * 0.2;

                fpsTimer = System.currentTimeMillis();
                fpsCounter = 0;
            } else {
                fpsCounter++;
            }

            Graphics2D g = engine.getGraphics();
            g.setColor(Color.RED);
            g.drawString("FPS: "+ fps, 2, 10);

            this.paintScreen();
        }
    }

    private void paintScreen() {
        final Graphics2D g2d;

        g2d = (Graphics2D) this.getGraphics();

        BufferedImage backBuffer = this.engine.getBackBuffer();
        if (g2d != null && (backBuffer != null))
        {
            g2d.drawImage(backBuffer, 0, 0, null);
        }
        Toolkit.getDefaultToolkit().sync();       // sync the display on some systems
        //g2d.dispose();
    }

    private void initLegacyInput() {
        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(Input.getDispatcher());
    }

    public abstract void loadScene();
}
