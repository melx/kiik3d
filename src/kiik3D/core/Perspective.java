package kiik3D.core;

import javax.media.opengl.glu.GLU;


public class Perspective {
    public double fovy = 45;
    public double aspectRatio;
    public double zNear = 0.1;
    public double zFar = 100.0;

    protected int width;
    protected int height;

    public Perspective() {
        setSize(640, 480);
    }

    public Perspective(int width, int height) {
        setSize(width, height);
    }

    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
        aspectRatio = (double)width/height;
    }

    public void set(GLU glu) {
        glu.gluPerspective(fovy, aspectRatio, zNear, zFar);
        //glu.gluOrtho2D( 0.0f, width, 0.0f, height);
    }

    public String toString() {
        return "<Perspective ("+fovy+", "+aspectRatio+", "+zNear+", "+zFar+")";
    }

}
