package kiik3D.core;


import java.util.HashMap;
import java.io.File;

import org.json.JSONException;

import kiik3D.objects.Entity;
import kiik3D.types.Mesh;
import kiik3D.util.Babylonloader;
import kiik3D.util.TextureReader;

import com.jogamp.opengl.util.texture.Texture;



public class ResourceManager {
    private static HashMap<String, Texture> textures = new HashMap<String, Texture>();
	private static HashMap<String, Mesh> models = new HashMap<String, Mesh>();

    public static TextureReader textureReader = new TextureReader();
    public static boolean texturesLoaded = false;

    public static void init() throws JSONException { //Create default prefabs
		loadModels();
    }
	
	public static Mesh getModel(String name) {
		if(models.containsKey(name)) {
			return models.get(name);
		}
		return null;
	}
	
	public static void loadModels() throws JSONException {
		String str;
		
		File dir = new File("./models");
		File[] listOfFiles = dir.listFiles();
		
		for (int j = 0; j < dir.listFiles().length; j++) {
			str = listOfFiles[j].getName();
			
			Mesh m = Babylonloader.loadModel("./models/" + str);
			
			str = str.replace(".babylon", "");
			
			models.put(str, m);
			
		} 
	
	}

    public static void loadTextures() {
        Texture tex = ResourceManager.textureReader.readTexture("textures/Suzanne.jpg");
        textures.put("Suzanne.jpg", tex);
        texturesLoaded = true;
        System.out.println("Textures loaded");
    }

    public static Texture getTexture(String textureName) {
        return (Texture)textures.get(textureName);
    }
}
