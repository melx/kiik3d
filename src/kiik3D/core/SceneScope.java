package kiik3D.core;


public class SceneScope {
    public static final ThreadLocal sceneThreadLocal = new ThreadLocal();

    public static void set(SceneContext scene) {
        sceneThreadLocal.set(scene);
    }

    public static void unset() {
        sceneThreadLocal.remove();
    }

    public static SceneContext get() {
        return (SceneContext)sceneThreadLocal.get();
    }

    public static SceneView getSceneView() {
        return get().scene;
    }

    public static Perspective getPerspective() {
        return get().scene.getPerspective();
    }

    public static Engine getEngine() {
        return get().engine;
    }
}
