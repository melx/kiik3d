package kiik3D.core;


public class SceneContext {
    public Engine engine;
    public SceneView scene;

    public SceneContext(Engine e, SceneView s) {
        engine = e;
        scene = s;
    }
}
