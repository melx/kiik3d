package kiik3D.core;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


import kiik3D.objects.Entity;
import kiik3D.types.*;


//Eeldefineeritud mänguobjektide kogum
public class EntityManager {
	private static HashMap<String, Entity> prefabs = new HashMap<String, Entity>();
    
    public static void init() {
		//Entity prefab = Babylonloader.getModelHashMap().get("monkey");
		Entity prefab = createMonkey("Suzanne");
		//  prefab.addComponent(new WASDMovement());
		prefabs.put("Suzanne", prefab);
    }
	
	public static Entity createMonkey(String name) {
        Entity e = new Entity(name);
        e.setMesh(ResourceManager.getModel("monkey"));
        return e;
    }

    public static Entity instantiate(String k) {
        Entity object = null;

        if(!prefabs.containsKey(k)) {
            return null;
        }

        try {
            object = instantiate(prefabs.get(k));
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        return object;
    }

    public static Entity instantiate(Entity prefab) throws java.io.IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(prefab);
        oos.flush();
        oos.close();
        bos.close();
        byte[] byteData = bos.toByteArray();

        ByteArrayInputStream bais = new ByteArrayInputStream(byteData);
        Entity object = null;
        try {
            object = (Entity) new ObjectInputStream(bais).readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        object.assignNewName();
        return object;
    }

    public static Mesh cubeMesh = new Mesh("cubeMesh",
            new Vertex[]{new Vertex(1, 1, 1),       //0
                        new Vertex(-1, 1, 1),     //1
                        new Vertex(-1, -1, 1),    //2
                        new Vertex(1, -1, 1),     //3
                        //Bottom (i=4)
                        new Vertex(1, 1, -1),     //4
                        new Vertex(-1, 1, -1),    //5
                        new Vertex(-1, -1, -1),   //6
                        new Vertex(1, -1, -1)     //7
                        },
            new Face[]{new Face(0, 1, 2),
                        new Face(2, 3, 0),
                        new Face(4, 5, 6),
                        new Face(6, 7, 4),
                        new Face(0, 1, 4),
                        new Face(4, 5, 1),

                        new Face(2, 3, 6),
                        new Face(3, 7, 6),

                        new Face(0, 3, 4),
                        new Face(3, 7, 4),
                        new Face(1, 2, 5),
                        new Face(2, 6, 5)

                        });

    public static Mesh triMesh = new Mesh("triMesh",
            new Vertex[]{new Vertex(1, 1, 0),
                        new Vertex(1, -1, 0),
                        new Vertex(-1, -1, 0),
                        new Vertex(-1, 1, 0),
                        new Vertex(0, 0, 3)
                        },
            new Face[]{});


    public static Entity createCube(String name) {
        Entity e = new Entity(name);
        e.setMesh(cubeMesh);

        return e;
    } 
    
}
