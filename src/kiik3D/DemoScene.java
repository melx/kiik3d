package kiik3D;

import kiik3D.core.EntityManager;
import kiik3D.core.ResourceManager;
import kiik3D.core.SceneView;
import kiik3D.types.Vec4;
import kiik3D.util.Babylonloader;
import kiik3D.objects.Entity;
import kiik3D.objects.Camera;
import kiik3D.components.WASDMovement;

import kiik3D.components.MonkeyAtom;


class DemoScene extends SceneView {

    public DemoScene() {
        super();
    }

    public void loadScene() {
        Entity kuubik = EntityManager.instantiate("Suzanne");
		kuubik.addComponent(new MonkeyAtom());

        kuubik.textureID = "suzanne.png";
        if(kuubik==null) {
            System.out.println("Failed to create object");
            return;
        }

        //mainCamera = new Camera(60);
        //mainCamera.Translate(-20, 0, 0);
        //System.out.println(mainCamera.getViewMatrix().toString());

        System.out.println("----");


        kuubik.setRotation(0, 0, 0);
        kuubik.setPosition(0, 0, 0);
        kuubik.setScale(1, 1, 1);
        kuubik.calcModelMatrix();

        System.out.println("Rotation: "+kuubik.getRotation().toString());
        System.out.println("Scale: "+kuubik.getScale().toString());
        System.out.println("Translation: "+kuubik.getTranslation().toString());
        System.out.println("TransformedVec: "+kuubik.getTransformedVec(new Vec4(1.0, 1.0, 1.0, 1.0)).toString());

        engine.spawnObject(kuubik);

        //System.out.println("GetCLass: "+kuubik.getComponent("kiik3D.components.DemoComponent"));

        Camera cam = new Camera();
        cam.addComponent(new WASDMovement());
        engine.setMainCamera(cam);

    }

    static int getRandCoord(int mx) {
        return randCoord(-mx, mx);
    }

    static int randCoord(int minRange, int maxRange) {
        return (int)((maxRange-minRange) * Math.random() + minRange);
    }
}
