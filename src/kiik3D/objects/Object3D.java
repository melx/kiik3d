package kiik3D.objects;

import kiik3D.core.Engine;
import kiik3D.components.Component;

import java.util.ArrayList;


public abstract class Object3D implements java.io.Serializable {
    protected String name;
    protected ArrayList<Component> components = new ArrayList<Component>();

    public abstract void update(Engine e);
    public abstract void Translate(double dX, double dY, double dZ);
    public abstract void Rotate(double aX, double aY, double aZ);
    public abstract void Scale(double sX, double sY, double sZ);

    public Object3D(String name) {
        this.name = name;
    }

    public Object3D() {
        name = "Unidentified";
    }

    public void addComponent(Component component) {
        components.add(component);
    }

    public Component getComponent(String compID) {
        Class compClass = null;
        try {
            compClass = Class.forName(compID);
        } catch (Throwable ex) {
            ex.printStackTrace();
            return null;
        }

        //TODO: check all components in list
        for(Component c : components) {
            if(compClass.isInstance(c)) {
                return c;
            }
        }

        return null;
    }

    public void updateComponents() {
        for(Component c : components) {
            c.update(this);
        }
    }
}
