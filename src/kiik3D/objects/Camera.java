package kiik3D.objects;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;

import kiik3D.core.Engine;
import kiik3D.types.Matrix4;
import kiik3D.types.Vec3;


public class Camera extends Object3D {
    private static int id_seq = 1;

    private Vec3 position = new Vec3(0, 0, 20);
    private Vec3 target = new Vec3(0, 0, 0);
    private Vec3 up = new Vec3(0, 1, 0);

    public Camera() {
        super("Camera #"+id_seq++);
    }

    public void Translate(double x, double y, double z) {
        position.X += x;
        position.Y += y;
        position.Z += z;
        /*
        target.X += x;
        target.Y += y;
        target.Z += z;
        */
    }

    public void Rotate(double x, double y, double z) {
        //System.out.println(target);
        //Matrix4 m = Matrix4.rotationMatrix(x, y, z);
        //target = m.multiply(target).normalize();
        //target.X = target.X + Math.sin(x) - Math.cos(x);
        //target.normalize();
        //System.out.println(target.X);
        target.X += x;
        target.Y += y;
        target.Z += z;
    }

    public void Scale(double x, double y, double z) {

    }

    public Vec3 getTarget() {
        return target;
    }

    public void update(Engine e) {
        updateComponents();
    }

    public void transform(GL2 gl, GLU glu) {
        //gl.gluLookAt(position.X, position.Y, position.Z, target.X, target.Y, target.Z, up.X, up.Y, up.Z);
        gl.glRotated(target.X, 1.0, 0.0, 0.0);
        gl.glRotated(target.Y, 0.0, 1.0, 0.0);
        gl.glRotated(target.Z, 0.0, 0.0, 1.0);
        gl.glTranslated(-position.X, -position.Y, -position.Z);
    }

}
