package kiik3D.objects;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import com.jogamp.opengl.util.texture.Texture;

import java.awt.Color;
import java.util.ArrayList;
import java.lang.reflect.*;

import kiik3D.core.Engine;
import kiik3D.core.SceneView;
import kiik3D.core.ResourceManager;

import kiik3D.input.Input;

import kiik3D.types.Matrix4;
import kiik3D.types.Vec2;
import kiik3D.types.Vec3;
import kiik3D.types.Vec4;
import kiik3D.types.Mesh;
import kiik3D.types.Face;
import kiik3D.types.Vertex;


public class Entity extends Object3D {
    private static int id_seq = 0;

    // Model View matrix variables
    protected Matrix4 translation;
    protected Matrix4 rotation;
    protected Matrix4 scale;
    protected Matrix4 modelMatrix;

    // Additional object references
    protected Mesh mesh;
    public String textureID = null;
    protected boolean transformChange;

    // Misc variables

    // Constructors
    public Entity(String name) {
        this.name = name;
        resetMatrix();
    }

    public Entity() {
        assignNewName();
        resetMatrix();
    }

    public void assignNewName() {
        this.name = "Entity "+(++id_seq);
    }

    // Constructors End

    public void setMesh(Mesh m) {
        mesh = m;
    }

    public void resetMatrix() {
        this.rotation = Matrix4.rotationMatrix(0, 0, 0);
        this.translation = Matrix4.translationMatrix(0, 0, 0);
        this.scale = Matrix4.scaleMatrix(1, 1, 1);
        transformChange = true;
    }


    // General methods TODO: translation by values
    public void Translate(double dX, double dY, double dZ) {
        translation.Translate(dX, dY, dZ);
        if(!transformChange) transformChange = true;
    }

    public void Rotate(double aX, double aY, double aZ) {
        rotation.Rotate(aX, aY, aZ);
        if(!transformChange) transformChange = true;
    }

    public void Scale(double sX, double sY, double sZ) {
        scale.Scale(sX, sY, sZ);
        if(!transformChange) transformChange = true;
    }

    public void setPosition(double x, double y, double z) {
        translation.toTranslation(x, y, z, 1);
        if(!transformChange) transformChange = true;
    }

    public void setScale(double sX, double sY, double sZ) {
        scale.toScale(sX, sY, sZ);
        if(!transformChange) transformChange = true;
    }

    public void setRotation(double aX, double aY, double aZ) {
        rotation.toRotation(aX, aY, aZ);
        if(!transformChange) transformChange = true;

    }

    public Vec4 getTransformedVec(Vec4 in_vec) {
        return translation.multiply(rotation.multiply(scale.multiply(in_vec)));
        //return modelMatrix.multiply(in_vec);
    }

    public void calcModelMatrix() {
        modelMatrix = (Matrix4)translation.multiply((Matrix4)rotation.multiply(scale));
    }


    // Getters
    public Matrix4 getModelMatrix() { return modelMatrix; }

    public Matrix4 getTranslation() { return this.translation; }

    public Matrix4 getRotation() { return this.rotation; }

    public Matrix4 getScale() { return this.scale; }


    public void legacyRender(Engine e) {
        if(transformChange) {
            if(mesh!=null) { //TODO: transform all connected meshes
                this.calcModelMatrix();
                mesh.transform(this.getModelMatrix());
                transformChange = false;
            }
        }

        Vertex[] transformedVertices = mesh.getTransformedVertices(); //FIXME: check if type should be Vec4 or Vec3
        Face[] faces = mesh.getFaces();

        Color color = null;
        int col = 0;
        for(int i=0;i<faces.length;i++) {
            Vertex[] verts = faces[i].getVertices(transformedVertices);

            switch(col) {
                case 0:
                    color = Color.BLUE;
                    col++;
                    break;
                case 1:
                    color = Color.WHITE;
                    col++;
                    break;
                case 2:
                    color = Color.RED;
                    col = 0;
                    break;

            }

            e.drawFace(verts, color);
        }
    }

    public void render(GL2 gl, Engine e) {
        gl.glMultMatrixd(translation.getValues(), 0);
        gl.glMultMatrixd(rotation.getValues(), 0);
        gl.glMultMatrixd(scale.getValues(), 0);

        Texture texture = ResourceManager.getTexture("Suzanne.jpg");
        if(texture!=null) {
            texture.enable(gl);
            texture.bind(gl);
        }

        gl.glBegin(GL.GL_TRIANGLES);
        int color = 0;

        Vec2 uvCoords;
        Vec3 pos;
        for(Face f : mesh.faces) {

            Vertex[] triangle = f.getVertices(mesh.vertices);
            for(Vertex v : triangle) {

				uvCoords = v.getUV();
                gl.glTexCoord2d(uvCoords.X, uvCoords.Y);
                
				pos = v.getPosition();
                gl.glVertex3d(pos.X, pos.Y, pos.Z);

			}
        }
        gl.glEnd();

        if(texture!=null) {
            texture.disable(gl);
        }
    }

    public void update(Engine e) {
        updateComponents();
    }

    public String toString() {
        return "<Entity '"+name+"'>";
    }
}
